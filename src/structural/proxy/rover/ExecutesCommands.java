package structural.proxy.rover;

public interface ExecutesCommands {

    public void execute(String command);
}

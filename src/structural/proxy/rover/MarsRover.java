package structural.proxy.rover;

import structural.adapter.Direction;
import structural.adapter.MarsEngine;
import structural.facade.Brakes;
import structural.facade.EngineCooler;

class MarsRover implements ExecutesCommands {

    MarsEngine engine;
    EngineCooler engineCooler;
    Brakes brakes;

    @Override
    public void execute(String command) {
        System.out.println("Rover got the command!:");
        System.out.println(command);

        if ("self-destruct".equalsIgnoreCase(command)) {
            System.exit(0);
        }
    }

    private void move(Direction direction) {
        engine.setDirecttion(direction);
        engineCooler.setCoolerMode(true);
        engine.driveForward(1);
        brakes.stopTheVehicle();
        engineCooler.setCoolerMode(false);
        System.out.println("Move process successfuly ended!");
    }
}

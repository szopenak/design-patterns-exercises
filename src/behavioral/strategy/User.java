package behavioral.strategy;

import java.math.BigDecimal;

public class User {
    private String name;
    private int age;
    private BigDecimal balance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public User(String name, int age, BigDecimal balance) {
        this.name = name;
        this.age = age;
        this.balance = balance;

    }
}

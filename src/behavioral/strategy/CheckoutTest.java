package behavioral.strategy;

import behavioral.strategy.payment.BitcoinPayment;
import behavioral.strategy.payment.PayPalPayment;
import behavioral.strategy.payment.ShortTermCreditPayment;
import org.junit.Test;
import structural.composite.ShoppingCart;
import structural.composite.products.Chocolate;
import structural.composite.products.Cola;
import structural.composite.products.Cookies;
import structural.composite.products.Snack;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CheckoutTest {
    @Test
    public void testUnderAgeClient(){
        User underAge = new User("Kiddo", 5, new BigDecimal("50"));

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Snack());
        shoppingCart.addProduct(new Chocolate());
        shoppingCart.addProduct(new Chocolate());

        Checkout checkout = new Checkout(shoppingCart, underAge);
        checkout.setPayment(new BitcoinPayment());
        boolean success = checkout.finalizeShopping();

        assertTrue(success);

//        assertEquals(underAge.getBalance()
//                .equals(new BigDecimal("50")
//                        .min(shoppingCart.getTotalCost())
//                        .min(shoppingCart.getTotalTax())
//                )
//        );
    }

    @Test
    public void testCreditPay() {
        User adulto = new User("Adulto", 22, new BigDecimal("0"));

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Snack());
        shoppingCart.addProduct(new Chocolate());
        shoppingCart.addProduct(new Chocolate());

        Checkout checkout = new Checkout(shoppingCart, adulto);
        checkout.setPayment(new ShortTermCreditPayment());

        boolean success = checkout.finalizeShopping();

        assertTrue(success);
        assertTrue(adulto.getBalance().equals(new BigDecimal("0")));
    }

    @Test
    public void testPayPalFailedForKid() {
        User underAge = new User("Kiddo", 5, new BigDecimal("50"));
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cola());

        Checkout checkout = new Checkout(shoppingCart, underAge);
        checkout.setPayment(new PayPalPayment());
        boolean success = checkout.finalizeShopping();

        assertFalse(success);
        assertEquals(underAge.getBalance(), new BigDecimal("50"));
    }
}

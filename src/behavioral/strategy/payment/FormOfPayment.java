package behavioral.strategy.payment;

import behavioral.strategy.User;

import java.math.BigDecimal;

public interface FormOfPayment {
    boolean pay(BigDecimal cash, User user);
}

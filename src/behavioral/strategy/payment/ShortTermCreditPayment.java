package behavioral.strategy.payment;

import behavioral.strategy.User;

import java.math.BigDecimal;

public class ShortTermCreditPayment implements FormOfPayment {
    @Override
    public boolean pay(BigDecimal cash, User user) {
        return false;
    }
}

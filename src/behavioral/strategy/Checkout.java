package behavioral.strategy;

import behavioral.strategy.payment.FormOfPayment;
import structural.composite.ShoppingCart;

public class Checkout {
    private ShoppingCart cart;
    private User user;
    private FormOfPayment payment;

    public Checkout(ShoppingCart cart, User user) {
        this.cart = cart;
        this.user = user;
    }

    public void setPayment(FormOfPayment payment) {
        this.payment = payment;
    }

    public boolean finalizeShopping(){
        // TODO : oblicz koszt calkowity z podatkiem

        // TODO : uzyj ustawiona forme platnosci oraz zwroc wynik true/false
        return true;
    }
}

package behavioral.template_method.laboratory;

import behavioral.template_method.laboratory.analyzer.AbstractSampleAnalyzeMethod;
import behavioral.template_method.laboratory.analyzer.SampleAnalyzeFactory;
import behavioral.template_method.laboratory.analyzer.report.Report;
import behavioral.template_method.laboratory.analyzer.sample.Sample;

import java.util.LinkedList;
import java.util.Observable;

public class ScientificLaboratory extends Observable {
    private LinkedList<Sample> samples = new LinkedList<>();

    public Report analyzeNextSample(){
        if (!samples.isEmpty()) {
            Sample sample = samples.removeFirst();
            AbstractSampleAnalyzeMethod sampleAnalyze = SampleAnalyzeFactory.getSampleAnalyze(sample);
            return sampleAnalyze.analyze();
        } else {
            return null;
        }
    }

    public void addSample(Sample s){
        samples.add(s);
    }

    public int getSamplesSize(){
        return samples.size();
    }
}

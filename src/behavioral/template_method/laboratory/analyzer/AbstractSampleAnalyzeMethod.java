package behavioral.template_method.laboratory.analyzer;

import behavioral.template_method.laboratory.analyzer.report.Report;
import behavioral.template_method.laboratory.analyzer.sample.Sample;

public abstract class AbstractSampleAnalyzeMethod {

    protected Sample sample;

    public final Report analyze() {
        System.out.println("Starting analysis of sample "+sample.getId());
        unWrap();
        prepareSample();
        Report report = analyzeSample();
        disposeSample();
        return report;
    }

    protected final void unWrap(){
        System.out.println("Unwrapping the sample "+sample.toString());
        this.sample.removeBox();
    }
    protected void disposeSample(){
        System.out.println("Removing the sample "+sample.toString());
        System.out.println("Sample dropped on the soil!");
        this.sample.removeBox();
    }

    public AbstractSampleAnalyzeMethod(Sample sample) {
        this.sample = sample;
    }

    protected abstract void prepareSample();
    protected abstract Report analyzeSample();
}

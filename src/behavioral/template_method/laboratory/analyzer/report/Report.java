package behavioral.template_method.laboratory.analyzer.report;

import java.time.LocalDate;

public class Report {
    private ReportStatus status;
    private String comment;
    private boolean surelyAlienStuff;
    private LocalDate analyzeDate = LocalDate.now();

    public Report(ReportStatus status, String comment, boolean surelyAlienStuff) {
        this.status = status;
        this.comment = comment;
        this.surelyAlienStuff = surelyAlienStuff;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public String getComment() {
        return comment;
    }

    public boolean isSurelyAlienStuff() {
        return surelyAlienStuff;
    }

    public LocalDate getAnalyzeDate() {
        return analyzeDate;
    }
}

package behavioral.template_method.laboratory.analyzer.report;

public enum ReportStatus {
    SUCCESS, FAILED, HARD_TO_SAY
}

package behavioral.template_method.laboratory.analyzer.sample;

public enum SampleStatus {
    OPENED, NOT_ANALYZED, DISPOSED
}

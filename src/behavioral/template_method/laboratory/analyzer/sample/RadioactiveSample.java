package behavioral.template_method.laboratory.analyzer.sample;

public class RadioactiveSample extends Sample {
    private boolean radioactive;

    public boolean isRadioactive() {
        return radioactive;
    }

    public void setRadioactive(boolean radioactive) {
        this.radioactive = radioactive;
    }
}

package behavioral.template_method.laboratory.analyzer.sample;

import java.util.Random;

public class Sample {
    private long id = new Random().nextLong();
    private SampleStatus status = SampleStatus.NOT_ANALYZED;

    public final void removeBox() {
        status = SampleStatus.OPENED;
    }

    public long getId() {
        return id;
    }
    public final void dispose() {
        status = SampleStatus.DISPOSED;
    }
}

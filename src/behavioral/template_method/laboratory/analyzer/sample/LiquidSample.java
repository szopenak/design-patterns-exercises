package behavioral.template_method.laboratory.analyzer.sample;

public class LiquidSample extends Sample {
    private int temperature;

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }
}

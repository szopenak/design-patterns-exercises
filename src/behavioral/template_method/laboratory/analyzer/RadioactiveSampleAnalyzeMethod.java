package behavioral.template_method.laboratory.analyzer;

import behavioral.template_method.laboratory.analyzer.report.Report;
import behavioral.template_method.laboratory.analyzer.sample.Sample;

public class RadioactiveSampleAnalyzeMethod extends AbstractSampleAnalyzeMethod {
    @Override
    protected void prepareSample() {
        // TODO : prepareSample
    }

    @Override
    protected Report analyzeSample() {
        // TODO : generate report
        return null;
    }

    public RadioactiveSampleAnalyzeMethod(Sample sample) {
        super(sample);
    }
}

package behavioral.template_method;

import behavioral.template_method.laboratory.ScientificLaboratory;
import behavioral.template_method.laboratory.analyzer.sample.GroundSample;
import behavioral.template_method.laboratory.analyzer.sample.LiquidSample;
import behavioral.template_method.laboratory.analyzer.sample.RadioactiveSample;

public class LaboratoryStaff {
    private ScientificLaboratory lab = new ScientificLaboratory();
    public static void main(String... args) {
        LaboratoryStaff laboratoryStaff = new LaboratoryStaff();

        laboratoryStaff.lab.addSample(new GroundSample());
        laboratoryStaff.lab.addSample(new LiquidSample());
        laboratoryStaff.lab.addSample(new RadioactiveSample());

        laboratoryStaff.lab.analyzeNextSample();
        laboratoryStaff.lab.analyzeNextSample();
        laboratoryStaff.lab.analyzeNextSample();
}
}

package behavioral.template_method;

import behavioral.observer.ReportObserver;
import behavioral.observer.SampleAmountObserver;
import behavioral.template_method.laboratory.ScientificLaboratory;
import behavioral.template_method.laboratory.analyzer.report.Report;
import behavioral.template_method.laboratory.analyzer.report.ReportStatus;
import behavioral.template_method.laboratory.analyzer.sample.GroundSample;
import behavioral.template_method.laboratory.analyzer.sample.LiquidSample;
import behavioral.template_method.laboratory.analyzer.sample.RadioactiveSample;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ScientificLaboratoryTest {

    @Test
    public void testAnalysisMethods(){
        ScientificLaboratory lab = new ScientificLaboratory();
        lab.addObserver(new SampleAmountObserver());
        lab.addObserver(new ReportObserver());

        GroundSample groundSample = new GroundSample();
        lab.addSample(groundSample);

        LiquidSample liquidSample = new LiquidSample();
        lab.addSample(liquidSample);

        RadioactiveSample radioactiveSample = new RadioactiveSample();
        lab.addSample(radioactiveSample);

        Report report1 = lab.analyzeNextSample();
        assertTrue("Normal ground sample report.".equals(report1.getComment()));
        assertTrue(ReportStatus.SUCCESS.equals(report1.getStatus()));
        assertFalse(report1.isSurelyAlienStuff());

        Report report2 = lab.analyzeNextSample();
        assertTrue("Water sample report!".equals(report2.getComment()));
        assertTrue(ReportStatus.SUCCESS.equals(report2.getStatus()));
        assertFalse(report2.isSurelyAlienStuff());
        assertEquals(liquidSample.getTemperature(), 100);

        Report report3 = lab.analyzeNextSample();
        assertTrue("Radioactive sample report! Really important".equals(report3.getComment()));
        assertTrue(ReportStatus.HARD_TO_SAY.equals(report3.getStatus()));
        assertFalse(report3.isSurelyAlienStuff());
        assertFalse(radioactiveSample.isRadioactive());
    }
}

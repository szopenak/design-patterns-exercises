package behavioral.visitor.products;

import structural.composite.PurchaseAble;

import java.math.BigDecimal;

public class PaintTheWall implements PurchaseAble{
    private int hours;
    private BigDecimal salaryPerHour = new BigDecimal("14");

    @Override
    public BigDecimal getPrice() {
        return salaryPerHour.multiply(new BigDecimal(hours));
    }

    public PaintTheWall(int hours) {
        this.hours = hours;
    }
}

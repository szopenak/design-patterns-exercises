package behavioral.visitor.products;

import structural.composite.PurchaseAble;

import java.math.BigDecimal;

public class Pendrive implements PurchaseAble{
    BigDecimal price = new BigDecimal("49");
    @Override
    public BigDecimal getPrice() {
        return price;
    }
}

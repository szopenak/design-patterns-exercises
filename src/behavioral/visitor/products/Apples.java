package behavioral.visitor.products;

import structural.composite.PurchaseAble;

import java.math.BigDecimal;

public class Apples implements PurchaseAble {

    private int weight;
    private BigDecimal costPerKg = new BigDecimal("3,69");

    @Override
    public BigDecimal getPrice() {
        return costPerKg.multiply(new BigDecimal(weight));
    }

    public Apples(int weight) {
        this.weight = weight;
    }
}

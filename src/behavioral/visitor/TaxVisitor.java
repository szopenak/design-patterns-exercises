package behavioral.visitor;

import behavioral.visitor.products.Apples;
import behavioral.visitor.products.PaintTheWall;
import behavioral.visitor.products.Pendrive;
import structural.composite.containers.Box;
import structural.composite.products.Chocolate;
import structural.composite.products.Cola;
import structural.composite.products.Cookies;
import structural.composite.products.Snack;

import java.math.BigDecimal;

public interface TaxVisitor {
    BigDecimal calculateTax(Snack snack);
    BigDecimal calculateTax(Chocolate chocolate);
    BigDecimal calculateTax(Box box);
    BigDecimal calculateTax(Cola cola);
    BigDecimal calculateTax(Cookies cookies);
    BigDecimal calculateTax(PaintTheWall paintTheWall);
    BigDecimal calculateTax(Apples apples);
    BigDecimal calculateTax(Pendrive pendrive);
}

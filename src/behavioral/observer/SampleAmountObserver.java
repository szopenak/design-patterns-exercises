package behavioral.observer;

import java.util.Observable;
import java.util.Observer;

public class SampleAmountObserver implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        // TODO : sprawdz czy arg to nie null i jest typu Report oraz czy o to obiekt typu ScientificLaboratory -> wypisz parametry raportu
    }
}

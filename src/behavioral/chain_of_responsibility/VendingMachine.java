package behavioral.chain_of_responsibility;

public class VendingMachine {

    CashDispenseHandler firstHandler;

    public void buyProduct(int actualPrice, int givenMoney) {
        firstHandler.dispenseCash(givenMoney - actualPrice);
    }

    public static void throwBackCash(int ammount, int coinValue) {
        System.out.println("Giving back "+ammount+" x "+coinValue+" coins!");
    }

    public VendingMachine() {
        // TODO : initialize chain of cashDispensers
    }
}

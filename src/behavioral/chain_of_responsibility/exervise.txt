#######################
####   Zadanie 14  ####
#######################
Dopisz do klasy VendingMachine część odpowiedzialną za wydawanie reszty pieniędzy z zakupu.
Zastosuj w tym przypadku wzorzec chain of responsibility według klasy abstrakcyjnej CashDispenserHandler. Założenia:
- wydawane monety są wymyślonej waluty i mają nominały: 100, 50, 25, 5, 1.
- kazdy z nominałów ma posiadać osobną implementację CashDispenseHandler, która wysyła wyliczoną ilość monet do metody statycznej
throwBackCash(int ammount, int coinValue) z klasy VendingMachine a następnie przekazuje resztę do następnego handlera
- jeśli pozostała reszta == 0, konkretny CashDispenseHandler nie powinien wołać kolejnego
- inicjalizacja wszystkich CashDispenseHandler i ich polączenie następuje w konstruktorze VendingMachine
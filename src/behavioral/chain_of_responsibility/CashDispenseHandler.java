package behavioral.chain_of_responsibility;

public abstract class CashDispenseHandler {
    private int coinValue;
    private CashDispenseHandler nextHandler;

    public void setNextHandler(CashDispenseHandler handler) {
        this.nextHandler = handler;
    }

    public abstract void dispenseCash(int val);
}

package creational.singleton;

import creational.builder.Meal;

public class SimpleFastFoodConsumer {

    // TODO : Upewnij sie, ze istnieje tylko jedna instancja tego obiektu!

    public void deliver(Meal meal) {
       // TODO :  zjedz posiłek
        meal.eat();
    }
}

package creational.builder;

public class Bacon {
    private int slices;

    public Bacon() {
        this.slices = 1;
    }

    public Bacon(int slices) {
        this.slices = slices;
    }

    public int getSlices() {
        return slices;
    }

    public void setSlices(int slices) {
        this.slices = slices;
    }
}

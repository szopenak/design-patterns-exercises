package creational.factory;

import creational.builder.Meal;

public abstract class BurgerFactory {
    public abstract Meal prepareBurger();
}

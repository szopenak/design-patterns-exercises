#######################
####   Zadanie 3   ####
#######################

Jesteś na etapie tworzenia hambuergerów -> dysponujesz klasą MealSimpleFactory, która powinna zwracać odpowiedni
typ burgera na podstawie przekazanego do niej argumentu.

1) Zaimplementuj kolejny rodzaj burgera - VeganBurger.class, który implemntuje interfejs Meal.class.
Wnętrze obiektu zaprojektuj według siebie (np. podobny do Hamburger.class, ale bez pola Cheese i z nowym obiektem
SoyMeat zamiast Meat)

2) Stwórz w MealSimpleFactory statyczną metodę wytwórczą, która na podstawie wczytanego z konsoli parametru
w formie String stworzy odpowiedni typ i rodzaj burgera -> obsłuż domyślny wybór lub przypadek gdy przekazano zły parametr

3) Wyślij otrzymany obiekt do wybranej implmentacji FastFoodConsumera

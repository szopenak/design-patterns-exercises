package creational.prototype.side_meals;

public class FryingOil {

    private Temperature temp;
    private boolean usedUp = false;

    public final void warmUp(){
            System.out.println("Warming up the oil!");
            temp = new Temperature();
            System.out.println("Oil is ready!");
    }

    public final void makeFries(Fries fries) {
        if (temp == null || !temp.isOk()) {
            System.out.println("No chance, oil is cold!");
            return;
        }
        if (usedUp) {
            System.out.println("No chance, oil is used up!");
            return;
        }
        this.usedUp = true;
        fries.setFried();
    }
}

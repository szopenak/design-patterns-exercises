package creational.prototype.side_meals;

public class Temperature {
    private int degrees = 0;
    public Temperature(){
        for (int i = 1; i <=5; i++) {
            try {
                Thread.sleep(1000);
                this.degrees+=40;
            } catch (InterruptedException e) {
                System.out.println("Someone has interrupted you!");
            }
            System.out.println("Temperature is "+degrees);
        }
    }
    public boolean isOk() {
        return degrees >= 200;
    }
}
